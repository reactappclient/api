var promise = require('bluebird');

var options = {
  promiseLib: promise
};

var pgp = require('pg-promise')(options);
var connectionString = 'postgres://postgres:haslodobazy@localhost:5432/GameBoard';
var db = pgp(connectionString);

function getAllThreads(req, res, next) {
  db.any('SELECT threads.id, threads.title, threads.date, users.login, COUNT(posts.id), users.avatar FROM threads LEFT JOIN users ON threads.id_user = users.id LEFT JOIN posts ON posts.id_thread = threads.id GROUP BY threads.id, users.login, users.avatar ORDER BY threads.date DESC')
    .then(function (data) {
      res.status(200)
        .json({
          status: 'success',
          data: data,
          message: 'Retrieved ALL threads'
        });
    })
    .catch(function (err) {
      return next(err);
    });
}

function getSingleThread(req, res, next) {
  var id = parseInt(req.params.id);
  db.one('select * from threads where id = $1', id)
    .then(function (data) {
      res.status(200)
        .json({
          status: 'success',
          data: data,
          message: 'Retrieved ONE thread'
        });
    })
    .catch(function (err) {
      return next(err);
    });
}


function createThread(req, res, next) {
  req.body.date = new Date(req.body.date);
  db.none('insert into threads (title, id_user, date)' +
      'values(${title}, ${author}, ${date})',
    req.body)
    .then(function(data) {
      db.one('select * from threads ORDER BY id DESC LIMIT 1').then(function (data) {
        res.status(200)
        .json({
          status: 'success',
          data: data,
          message: 'Inserted one thread'
        });
      })
    })
    .catch(function (err) {
      return next(err);
    });
}

function updateThread(req, res, next) {
  db.none('update threads set title=$1 where id=$2',
    [req.body.title, parseInt(req.params.id)])
    .then(function () {
      res.status(200)
        .json({
          status: 'success',
          message: 'Updated thread'
        });
    })
    .catch(function (err) {
      return next(err);
    });
}

function removeThread(req, res, next) {
  var id = parseInt(req.params.id);
  db.result('delete from threads where id = $1', id)
    .then(function (result) {
      res.status(200)
        .json({
          status: 'success',
          message: `Removed ${result.rowCount} User`
        });
    })
    .catch(function (err) {
      return next(err);
    });
}


function getAllUsers(req, res, next) {
  db.any('select * from users')
    .then(function (data) {
      res.status(200)
        .json({
          status: 'success',
          data: data,
          message: 'Retrieved ALL Users'
        });
    })
    .catch(function (err) {
      return next(err);
    });
}

function getSingleUser(req, res, next) {
  var id = parseInt(req.params.id);
  db.one('select * from users where id = $1', id)
    .then(function (data) {
      res.status(200)
        .json({
          status: 'success',
          data: data,
          message: 'Retrieved ONE User'
        });
    })
    .catch(function (err) {
      return next(err);
    });
}

function createUser(req, res, next) {
  db.none('insert into users(login, password, avatar)' +
      'values(${login}, ${password}, ${avatar})',
    req.body)
    .then(function () {
      res.status(200)
        .json({
          status: 'success',
          message: 'Inserted one User'
        });
    })
    .catch(function (err) {
      return next(err);
    });
}

function updateUser(req, res, next) {
  db.none('update users set login=$1, password=$2 where id=$3',
    [req.body.login, req.body.password, parseInt(req.params.id)])
    .then(function () {
      res.status(200)
        .json({
          status: 'success',
          message: 'Updated User'
        });
    })
    .catch(function (err) {
      return next(err);
    });
}

function removeUser(req, res, next) {
  var id = parseInt(req.params.id);
  db.result('delete from users where id = $1', id)
    .then(function (result) {
      res.status(200)
        .json({
          status: 'success',
          message: `Removed ${result.rowCount} User`
        });
    })
    .catch(function (err) {
      return next(err);
    });
}



function getPostsByThreadId(req, res, next) {
  var id_thread = parseInt(req.params.id_thread);
  db.any('SELECT posts.id, posts.content, posts.date, users.login, users.id as userId, users.avatar FROM posts LEFT JOIN users ON posts.id_user = users.id where posts.id_thread = $1', id_thread)
    .then(function (data) {
      res.status(200)
        .json({
          status: 'success',
          data: data,
          message: 'Retrieved posts'
        });
    })
    .catch(function (err) {
      return next(err);
    });
}

function getSinglePost(req, res, next) {
  var id = parseInt(req.params.id);
  db.one('select * from posts where id = $1', id)
    .then(function (data) {
      res.status(200)
        .json({
          status: 'success',
          data: data,
          message: 'Retrieved ONE post'
        });
    })
    .catch(function (err) {
      return next(err);
    });
}

function createPost(req, res, next) {
  req.body.date = new Date(req.body.date);
  db.none('insert into posts(id_user, id_thread, content, date)' +
      'values(${id_user}, ${id_thread}, ${content}, ${date})',
    req.body)
    .then(function () {
      res.status(200)
        .json({
          status: 'success',
          message: 'Inserted one post'
        });
    })
    .catch(function (err) {
      return next(err);
    });
}

function updatePost(req, res, next) {
  db.none('update posts set content=$1, where id=$2',
    [req.body.content, parseInt(req.params.id)])
    .then(function () {
      res.status(200)
        .json({
          status: 'success',
          message: 'Updated Post'
        });
    })
    .catch(function (err) {
      return next(err);
    });
}

function removePost(req, res, next) {
  var id = parseInt(req.params.id);
  db.one('delete from posts where id = $1', id)
    .then(function (data) {
      if (data != null) {
      res.status(200)
        .json({
          status: 'success',
          data: data,
          message: 'Retrieved ONE post'
        });
      } else {
        res.status(200)
        .json({
          status: 'error',
        });
      }
    })
    .catch(function (err) {
      return next(err);
    });
}



function login(req, res, next) {
  db.one('select * from users where login = ${login} and password = ${password}', req.body)
    .then(function (data) {
      console.log(data);
      res.status(200)
        .json({
          status: 'success',
          data: data,
        });
    })
    .catch(function (err) {
      return next(err);
    });
}


module.exports = {
  getAllThreads: getAllThreads,
  getSingleThread: getSingleThread,
  createThread: createThread,
  updateThread: updateThread,
  removeThread: removeThread,
  
  getAllUsers: getAllUsers,
  getSingleUser: getSingleUser,
  createUser: createUser,
  updateUser: updateUser,
  removeUser: removeUser,

  getPostsByThreadId: getPostsByThreadId,
  getSinglePost: getSinglePost,
  createPost: createPost,
  updatePost: updatePost,
  removePost: removePost,

  login: login,
};