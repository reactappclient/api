CREATE TABLE users ( id SERIAL PRIMARY KEY, login VARCHAR, password VARCHAR, avatar VARCHAR );

INSERT INTO users (login, password, avatar) VALUES ('Ali', 'mojeTajneHaslo', 'https://i.ytimg.com/vi/orFojS02hN0/maxresdefault.jpg');