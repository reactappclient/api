var express = require('express');
var router = express.Router();
var db = require('../queries');

router.get('/api/threads', db.getAllThreads);
router.get('/api/threads/:id', db.getSingleThread);
router.post('/api/threads', db.createThread);
router.put('/api/threads/:id', db.updateThread);
router.delete('/api/threads/:id', db.removeThread);

router.get('/api/users', db.getAllUsers);
router.get('/api/users/:id', db.getSingleUser);
router.post('/api/users', db.createUser);
router.put('/api/users/:id', db.updateUser);
router.delete('/api/users/:id', db.removeUser);

router.get('/api/posts/:id_thread', db.getPostsByThreadId);
router.post('/api/posts', db.createPost);
router.put('/api/posts/:id', db.updatePost);
router.delete('/api/posts/:id', db.removePost);

router.post('/api/login', db.login);


router.get('/', function (req, res) {
  res.render('index', {title: 'node-postgres-promises'}); 
});

module.exports = router;